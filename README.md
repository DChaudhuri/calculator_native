# README
# Preview
![Preview](https://firebasestorage.googleapis.com/v0/b/squery-e375a.appspot.com/o/rsz_1screenshot_1588311484.png?alt=media&token=e86ae583-2118-40f0-baf5-080078a8fe06)
## Requirements :
1. Node
2. React Native
3. Detox
4. Android Studio

### Steps to run :
1. `git clone https://gitlab.com/DChaudhuri/calculator_native.git`
2. `cd calculator_native`
3. `npm install`
4. `npm install -g detox-cli`
5. `npx react-native run-android --variant=release`

### For testing we have used detox

### Heads up :
It is recommended that you have android emulator Nexus 5X API 29 x86 for testing otherwise you just need to change the avdname of the emulator under detox configuration in package.json to the one that you have.
`"device": {
          "avdName": "Nexus_5X_API_29_x86"
}`

### Steps to test(debug mode) :
1. `detox build -c android.emu.debug`
2. `detox test -c android.emu.debug`

### Steps to test(release mode) :
1. `detox build -c android.emu.release`
2. `detox test -c android.emu.release`

### Steps to add the shell as your gitlab runner :

1. Install gitlab runner https://docs.gitlab.com/runner/install/  from the Binaries section depending on your system.
2. Obtain a token which will be used for registering your runner. For using a specific runner, go to the settings -> CI/CD . Expand the runners section.
3. Register the runner https://docs.gitlab.com/runner/register/.
4. Now when your terminal asks “ Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com ) “ enter https://gitlab.com 
5. For the question “Please enter the gitlab-ci token for this runner” provide the token which is given in the settings -> CI/CD under runners section Set up a specific Runner manually part
6. Enter your runner name as my-runner.
7. Specify the tags based on your project. For example, if your project is javascript oriented then use the tag javascript.
8. Enter the runner executor as shell as you want to use your terminal as the runner.
9. Now in your terminal run the command sudo gitlab-runner run.
10. Your runner is set up.
