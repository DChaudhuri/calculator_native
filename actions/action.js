export const operator=()=>{
    return({
        type:'REQUEST_EQUAL_TO'
    })
}
export const storeData=(data)=>{
    return({
        type:'REQUEST_STORE_DATA',payload:data
    })
}
export const clearAll=()=>{
    return({
        type:'RESPONSE_CLEAR_ALL'
    })
}
export const del=()=>{
    return({
        type:'RESPONSE_DELETE'
    })
}