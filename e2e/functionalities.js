export const Functionalities=()=>{
    it('Performs Subtraction',async()=>{
        const button1=element(by.id("1"));
        await button1.tap();
        const operator=element(by.id("-"));
        await operator.tap();
        const button2=element(by.id("3"));
        await button2.tap();
        const operation=element(by.id("="));
        await operation.tap();
        const message=element(by.id('inputView'));
        await expect(message).toHaveText("-2")
    })
    it('Performs Clear',async()=>{
        const clear=element(by.id("C"));
        await clear.tap();
        const message=element(by.id('inputView'));
        await expect(message).toHaveText("")
    })
    it('Performs Division',async()=>{
        const button1=element(by.id("4"));
        await button1.tap();
        const operator=element(by.id("/"));
        await operator.tap();
        const button2=element(by.id("2"));
        await button2.tap();
        const operation=element(by.id("="));
        await operation.tap();
        const message=element(by.id('inputView'));
        await expect(message).toHaveText("2")
    })
    it('Performs Clear',async()=>{
        const clear=element(by.id("C"));
        await clear.tap();
        const message=element(by.id('inputView'));
        await expect(message).toHaveText("")
    })
    it('Performs Multiplication',async()=>{
        const button1=element(by.id("3"));
        await button1.tap();
        const operator=element(by.id("*"));
        await operator.tap();
        const button2=element(by.id("3"));
        await button2.tap();
        const operation=element(by.id("="));
        await operation.tap();
        const message=element(by.id('inputView'));
        await expect(message).toHaveText("9")
    })
    it('Performs Clear',async()=>{
        const clear=element(by.id("C"));
        await clear.tap();
        const message=element(by.id('inputView'));
        await expect(message).toHaveText("")
    })
    it('Performs Addition',async()=>{
        const button1=element(by.id("1"));
        await button1.tap();
        const operator=element(by.id("+"));
        await operator.tap();
        const button2=element(by.id("3"));
        await button2.tap();
        const operation=element(by.id("="));
        await operation.tap();
        const message=element(by.id('inputView'));
        await expect(message).toHaveText("4")
    })
    it('Performs Clear',async()=>{
        const clear=element(by.id("C"));
        await clear.tap();
        const message=element(by.id('inputView'));
        await expect(message).toHaveText("")
    })
    it('Performs Delete',async()=>{
        const button1=element(by.id("3"));
        await button1.tap();
        const button2=element(by.id("4"));
        await button2.tap();
        const button3=element(by.id("5"));
        await button3.tap();
        const message1=element(by.id('inputView'));
        await expect(message1).toHaveText("345")
        const deleteBtn=element(by.id("Del"));
        await deleteBtn.tap();
        const message2=element(by.id('inputView'));
        await expect(message2).toHaveText("34")
    })
    it('Performs Clear',async()=>{
        const clear=element(by.id("C"));
        await clear.tap();
        const message=element(by.id('inputView'));
        await expect(message).toHaveText("")
    })
    it('Division by zero is not possible',async()=>{
        const button1=element(by.id("1"));
        await button1.tap();
        const operator=element(by.id("/"));
        await operator.tap();
        const button2=element(by.id("0"));
        await button2.tap();
        const message=element(by.id('inputView'));
        await expect(message).toHaveText("1/")
    })
}