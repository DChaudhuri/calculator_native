import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { TouchableOpacity,Text } from "react-native";
import { storeData,operator,clearAll,del } from "../../../actions/action";
import { styles } from "./styles";

const Button = ({item,storeData,operator,clearAll,del}) =>{
    
    return(
            <TouchableOpacity 
                testID={`${item}`}
                style={[
                    styles.button,
                    typeof item==='string'? styles.grey:styles.white ,
                    item==='='?styles.equal:styles.normal,
                ]} 
                onPress={
                    item==='Del' ? del : item==='=' ? operator : item==='C' ? clearAll : ()=>storeData(item)
                }>

                <Text
                
                    style={[
                        styles.text,
                        typeof item==='string' && styles.coloredText,
                        item==='=' && styles.whiteText
                    ]}
                >{item}</Text>
            </TouchableOpacity>
    )
}

const mapDispatchToProps=(dispatch)=>{
    return bindActionCreators({
        storeData,operator,clearAll,del
    },dispatch)
}
export default connect(null,mapDispatchToProps)(Button);