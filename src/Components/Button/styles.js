import { StyleSheet } from "react-native";
export const styles=StyleSheet.create({
    button:{
        display:'flex',
        borderWidth:0.5,
        borderColor:'#bbbbbb',
        justifyContent:'center',
        alignItems:'center'
    },
    equal:{
        height:'40%',
        width:'25%',
        backgroundColor:'#21aded',
    },
    normal:{
        height:'20%',
        width:'25%',
    },
    grey:{
        backgroundColor:'#f7f7f7'
    },
    white:{
        backgroundColor:'white'
    },
    text:{
        fontSize:30,
    },
    coloredText:{
        color:'#21aded'
    },
    whiteText:{
        color:'white'
    }
})