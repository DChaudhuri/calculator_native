import { StyleSheet } from "react-native";
export const styles=StyleSheet.create({
    buttonContainer:{
        display:'flex',
        flex:1,
        flexWrap:'wrap',
        minHeight:300
    },
})