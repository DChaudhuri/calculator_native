import React from "react";
import {  View } from "react-native";
import Button from "../../Components/Button/index";
import { styles } from "./styles";

const arr=['C',7,4,1,'','/',8,5,2,0,'*',9,6,3,'.','Del','-','+','='];

const CalcButton=(props)=>{
    
    return(
        <View style={styles.buttonContainer}>
            {
                arr.map((item,index)=>{
                    return(
                        <Button key={index} item={item} />
                    )
                })
            }
        </View>
    )
}
export default (CalcButton);