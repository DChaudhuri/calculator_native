import React from "react";
import { connect } from "react-redux";
import { View, Text } from "react-native";
import { selectInputString } from "../../../selectors/selector";
import { styles }  from "./styles";

const InputView=(props)=>{
    console.log(props)
    return(
        <View style={styles.container}>
            <View  style={styles.inputContainer}>
                <Text testID="inputView"  style={styles.inputText}>{props.inputString}</Text>
            </View>
        </View>
    )
}
const mapStateToProps=(store)=>{
    return({
        inputString:selectInputString(store),
    })
}
export default connect(mapStateToProps,null)(InputView);