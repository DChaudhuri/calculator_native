import { StyleSheet } from "react-native";
export const styles=StyleSheet.create({
    inputContainer:{
        display:'flex',
        flexBasis:'30%',
        justifyContent:'flex-end',
    },
    inputText:{
        textAlign:'right',
        fontSize:55
    }
})