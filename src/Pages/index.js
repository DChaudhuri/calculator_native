import React from "react";
import { View } from "react-native";
import InputView from "./InputView";
import CalcButton from "./CalcButton";
import { styles }  from "./styles";

const Calculator=()=>{
    return(
        <View style={styles.container}>
            <InputView />
            <CalcButton />
        </View>
    )
}
export default (Calculator);