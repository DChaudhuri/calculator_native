import { put,takeLatest,select } from "redux-saga/effects";
export function* watcherSaga(){
    yield takeLatest('REQUEST_EQUAL_TO',operation)
    yield takeLatest('REQUEST_STORE_DATA',storeData)
}
function* operation(){
    const { inputString }=yield select();
    let calcString;
    try{
        calcString=eval(inputString).toString();
    }catch(e){
        calcString="error";
    }
    yield put({
        type:'RESPONSE_EQUAL_TO',
        payload:calcString
    });
}

function* storeData(action){
    const { inputString }=yield select();
    if(inputString.charAt(inputString.length-1)=='/' && action.payload=='0'){
        yield put({
            type:'RESPONSE_STORE_DATA',
            payload:''
        })
    }
    else{
        yield put({
            type:'RESPONSE_STORE_DATA',
            payload:action.payload
        })
    }
}