import { createSelector } from "reselect";
const selectInput = store => store.inputString
export const selectInputString = createSelector(
    selectInput,
    (inputString)=>{
        return inputString;
    }
)