/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { Provider } from "react-redux";
import logger from 'redux-logger';
import { createStore,applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";

import { watcherSaga } from "./sagas/saga";
import { reducer } from "./reducer/reducer";
import Calculator from './src/Pages';
const sagaMiddleware=createSagaMiddleware();
const store=()=>createStore(reducer,applyMiddleware(sagaMiddleware,logger));
const _store=store();
sagaMiddleware.run(watcherSaga);
const App=() => {
  return (
    <>
      <Provider store={_store}>
        <Calculator />
      </Provider>
    </>
  );
};

export default App;
