const initialState={
    inputString:'',
}
export const reducer=(state=initialState,action)=>{
    switch(action.type){
        case 'RESPONSE_STORE_DATA':return{
            ...state,inputString:state.inputString.concat(action.payload)
        }
        case 'RESPONSE_EQUAL_TO':
            return{
            ...state,inputString:action.payload
        }
        case 'RESPONSE_CLEAR_ALL':return{
            ...state,inputString:''
        }
        case 'RESPONSE_DELETE':return{
            ...state,inputString:state.inputString.substring(0,state.inputString.length-1)
        }
        default:return state;
    }
}